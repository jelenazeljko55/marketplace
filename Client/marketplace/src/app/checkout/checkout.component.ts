import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CartService } from '../services/cart.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  id: number = 0;
  constructor( public activeRoute: ActivatedRoute, private cartService: CartService) {
    this.activeRoute.params.subscribe(params => {
      this.id = params['id'];
      this.submit
    });
   }

  ngOnInit(): void {
  }

  submitForm(){
    console.log("ffsa");
  }

  submit(){
    this.cartService.getSumbit(this.id).subscribe( (retVal: string) => {
      alert(retVal);
    })
  }

  

}
