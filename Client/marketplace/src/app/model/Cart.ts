export class Cart{
    id: number;
    products: [];

    constructor(id: number, products: []){
        this.id = id;
        this.products = products;
    }
}