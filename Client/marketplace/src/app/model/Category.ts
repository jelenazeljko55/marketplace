export class Category{
    id: number;
    name: string;
    pictureUrl: string;
    description: string;
    productList : [];

    constructor(id: number, name: string, pictureUrl: string, description: string, productList: []){
        this.id = id;
        this.name = name;
        this.pictureUrl = pictureUrl;
        this.description = description;
        this.productList = productList;
    }
}