import { Category } from "./Category";

export class Product{
    id: number;
    name: string;
    description : string;
    price: number;
    pictureUrl: string;
    quantity: number;
    dateAndTime: string;
    deleted: boolean;
    category: Category;

    constructor(id: number, name: string, description: string, price: number, pictureUrl: string, quantity: number, dateAndTime: string, deleted: boolean, category: Category){
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.pictureUrl = pictureUrl;
        this.quantity = quantity;
        this.dateAndTime = dateAndTime;
        this.deleted = deleted;
        this.category = category;
    }
}