export class ProductQuantity{
    productId: number;
	productName: string;
	productUrl: string;
    quantityInCart: number;

    constructor(productId: number, productName: string, productUrl: string, quantityInCart: number){
        this.productId = productId;
        this.productName = productName;
        this.productUrl = productUrl;
        this.quantityInCart = quantityInCart;
    }

}