import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryService } from '../services/category.service';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit{

  products: any= [ {
    name: "YYY",
    id: 1,
    pictureUrl: "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg",
    quantity: 8
  }
];

  cartId: number = 0;

  categories: any = [];

  categoryInputValue: string = '';

  constructor(private productService: ProductService, private categoryService: CategoryService, private router: Router) { }

  ngOnInit(): void {
    this.getProducts();
    this.getCategories();
    
    
  }
  

  getProducts(){
    this.productService.getProductsServiceMethod().subscribe( (retVal: any) => {
      this.products = retVal;
    });
  }

  getCategories(){
    this.categoryService.getCategories().subscribe( (retVal: any) => {
      this.categories = retVal;
    });
  }

  addItemToCart(productId:number){
    this.productService.addItemToCart(productId, this.cartId).subscribe( (retVal) => {
      this.cartId = retVal;
      console.log(retVal);
    });
  }

  goToDetails(){
    this.router.navigateByUrl('details-product');
  }

  getProductByCategory(value: string){
    this.productService.getProductsByCategory(this.categoryInputValue).subscribe( (retVal: string) => {
      this.products= retVal;
    })
  }

}
