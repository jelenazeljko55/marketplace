import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProductQuantity } from '../model/ProductQuantity';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  url: string = 'http://localhost:8080/api/carts';

  url1: string = 'http://localhost:8080/api/carts/buy/';

  constructor(private http: HttpClient) { }

  getCartProduct(valueId: number) : Observable<ProductQuantity[]>{
    return this.http.get<ProductQuantity[]>(this.url +"/" + valueId);
  }

  getSumbit(id: number) : Observable<any>{
    return this.http.get(this.url1 + id);
  }
}
