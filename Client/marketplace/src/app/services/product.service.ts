import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  url: string = 'http://localhost:8080/api/products';
  urlCart: string = 'http://localhost:8080/api/carts';


  constructor(private http: HttpClient) { }
    
    getProductsServiceMethod(): Observable<any> {
      return this.http.get(this.url);
    }

    addItemToCart(productId: number, cartId: number) : Observable<any>{
      const params = new HttpParams()
        .set('productId', productId)
        .set('cartId', cartId);
      return this.http.get(this.urlCart, {params});
    }

    getProductsByCategory(value: string) : Observable<any> {
      console.log(value + "===");
      return this.http.get(this.url + '/byCategory/' + value);
    }
  
}
