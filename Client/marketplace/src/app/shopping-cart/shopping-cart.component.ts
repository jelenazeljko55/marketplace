import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from '../model/Product';
import { ProductQuantity } from '../model/ProductQuantity';
import { CartService } from '../services/cart.service';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {

productsInCart: ProductQuantity[] = [];

 public id: number = 0;

  constructor(public cartService: CartService, public activeRoute: ActivatedRoute,  private router: Router) {
    this.activeRoute.params.subscribe(params => {
        this.id = params['id'];
        this.getCart();
      });
  }

  ngOnInit(): void {
  } 
  

  getCart(){
    this.cartService.getCartProduct(this.id).subscribe( (retVal: any) => {
      this.productsInCart = retVal;
      console.log(retVal);
    });
  }

  goToCheckOut(){
    this.router.navigateByUrl('checkout');
  }

}
