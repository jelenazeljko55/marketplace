//package app.marketplace;
//
//import java.time.LocalDateTime;
//
//import javax.annotation.PostConstruct;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import app.marketplace.model.Category;
//import app.marketplace.model.Product;
//import app.marketplace.service.CategoryService;
//import app.marketplace.service.ProductService;
//
//@Component
//public class InitialData {
//	
//	@Autowired
//	ProductService productService;
//	
//	@Autowired
//	CategoryService categoryService;
//	
//	
//
//	@PostConstruct
//	public void init() {
//		
//		Category c1 = new Category("trenerka","trenerka","https://cdn.bike24.net/i/mb/d8/50/a6/nike-sportswear-older-kids-tracksuit-black-black-white-bv3634-010-2-851717.jpg");
//		categoryService.save(c1);
//		
//		Category c2 = new Category("sorc","sorc","https://images.sportsdirect.com/images/products/63034740_l.jpg");
//		categoryService.save(c2);
//		
//		Category c3 = new Category("pantalone","pantalone jeans","https://m.media-amazon.com/images/I/91jz+aqpMvL._AC_UL1500_.jpg");
//		categoryService.save(c3);
//		
//		Category c4 = new Category("torbica","torbica","https://media.gucci.com/style/DarkGray_Center_0_0_980x980/1623241803/360_400249_KHNRN_8642_001_100_0000_Light-Dionysus-small-GG-shoulder-bag.jpg");
//		categoryService.save(c4);
//		
//		Category c5 = new Category("kacket","kacket","https://www.sportvision.rs/files/thumbs/files/images/slike_proizvoda/media/AV8/AV8055-657/images/thumbs_800/AV8055-657_800_800px.jpg");
//		categoryService.save(c5);
//		
//		
//		Product p1 = new Product( "trenerka nike", "https://cdn.bike24.net/i/mb/d8/50/a6/nike-sportswear-older-kids-tracksuit-black-black-white-bv3634-010-2-851717.jpg", 10, "trenerka nike", 14500.00,false, LocalDateTime.now(), c1);
//		productService.save(p1);
//		
//		Product p2 = new Product( "sorc ua", "https://images.sportsdirect.com/images/products/63034740_l.jpg", 10, "ua sorc", 1500.00,false, LocalDateTime.now(), c2);
//		productService.save(p2);
//		
//		Product p3 = new Product( "pantalone boss", "https://m.media-amazon.com/images/I/91jz+aqpMvL._AC_UL1500_.jpg", 10, "hugo boss pantalone", 15500.00,false, LocalDateTime.now(), c3);
//		productService.save(p3);
//		
//		Product p4 = new Product( "torbica gucci", "https://media.gucci.com/style/DarkGray_Center_0_0_980x980/1623241803/360_400249_KHNRN_8642_001_100_0000_Light-Dionysus-small-GG-shoulder-bag.jpg", 10, "gucci torbica", 145400.00,false, LocalDateTime.now(), c4);
//		productService.save(p4);
//		
//		Product p5 = new Product( "kacket nike", "https://www.sportvision.rs/files/thumbs/files/images/slike_proizvoda/media/AV8/AV8055-657/images/thumbs_800/AV8055-657_800_800px.jpg", 10, "crveni kacket", 14500.00,false, LocalDateTime.now(), c5);
//		productService.save(p5);
//		
//	}
//}
