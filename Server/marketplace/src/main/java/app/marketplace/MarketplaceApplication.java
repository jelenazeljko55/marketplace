package app.marketplace;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarketplaceApplication {
	
//	@Autowired
//	InitialData initialData;
	
	public static void main(String[] args) {
		SpringApplication.run(MarketplaceApplication.class, args);
	}

}
