package app.marketplace.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import app.marketplace.dto.CartDTO;
import app.marketplace.dto.ProductQuantityDTO;
import app.marketplace.model.Cart;
import app.marketplace.service.CartService;

@CrossOrigin("*")
@RestController
@RequestMapping(value = "api/carts")
public class CartController {

	@Autowired
	CartService cartService;

	@GetMapping
	public ResponseEntity<Long> addToCart(@RequestParam Long productId, @RequestParam Long cartId) throws Exception {
		Long id = cartService.addToCart(productId, cartId);

		return new ResponseEntity<>(id, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<List<ProductQuantityDTO>> getById(@PathVariable Long id) {
		Cart cart = cartService.getById(id);
		CartDTO dto = new CartDTO(cart);

		return new ResponseEntity<>(dto.getProductQuantityDTOs(), HttpStatus.OK);
	}

	@PutMapping(value = "/buy/{cartId}")
	public ResponseEntity<?> buy(@PathVariable Long cartId) {
		String message = cartService.buy(cartId);

		return new ResponseEntity<String>(message, HttpStatus.OK);
	}

}
