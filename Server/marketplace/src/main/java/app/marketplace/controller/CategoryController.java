package app.marketplace.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import app.marketplace.dto.CategoryDTO;
import app.marketplace.model.Category;
import app.marketplace.service.CategoryService;

@CrossOrigin("*")
@RestController
@RequestMapping(value = "api/categories")
public class CategoryController {

	@Autowired
	CategoryService categoryService;

	@GetMapping
	public ResponseEntity<List<CategoryDTO>> getCategories(
			@RequestParam(required = false, defaultValue = "") String name) {

		List<Category> categories = new ArrayList<>();
		ArrayList<CategoryDTO> dtos = new ArrayList<>();

		if (name.isEmpty()) {
			categories = categoryService.findAll();

			for (Category category : categories)
				dtos.add(new CategoryDTO(category));

			return new ResponseEntity<>(dtos, HttpStatus.OK);
		}

		categories = categoryService.findByNameContaining(name);

		for (Category category : categories) {
			dtos.add(new CategoryDTO(category));
		}

		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<CategoryDTO> getOneCategory(@PathVariable Long id) {
		Optional<Category> category = categoryService.findOne(id);

		if (!category.isPresent())
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		CategoryDTO categoryDTO = new CategoryDTO(category.get());
		return new ResponseEntity<>(categoryDTO, HttpStatus.OK);

	}
}
