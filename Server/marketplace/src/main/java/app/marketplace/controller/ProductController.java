package app.marketplace.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import app.marketplace.dto.ProductDTO;
import app.marketplace.dto.ProductPageDTO;
import app.marketplace.mapper.ProductMapper;
import app.marketplace.model.Product;
import app.marketplace.service.ProductService;

@CrossOrigin("*")
@RestController
@RequestMapping(value = "api/products")
public class ProductController {

	@Autowired
	ProductService productService;

	@Autowired
	ProductMapper productMapper;

	@GetMapping(params = { "page", "size" })
	public ResponseEntity<ProductPageDTO> getProductsPaged(Pageable pageable) {
		Page<Product> products = productService.findAll(pageable);
		ProductPageDTO pageDto = new ProductPageDTO(products);

		return new ResponseEntity<>(pageDto, HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<List<ProductDTO>> getProducts(
			@RequestParam(required = false, defaultValue = "") String name) {

		List<Product> products = new ArrayList<>();
		ArrayList<ProductDTO> dtos = new ArrayList<>();

		if (name.isEmpty()) {
			products = productService.findAll();

			for (Product product : products) {
				if (product.isDeleted() == false)
					dtos.add(new ProductDTO(product));
			}
			Collections.reverse(dtos);
			return new ResponseEntity<>(dtos, HttpStatus.OK);
		}

		products = productService.findByNameContaining(name);

		for (Product product : products)
			dtos.add(new ProductDTO(product));

		Collections.reverse(dtos);
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}

	@GetMapping(value = "/byCategory/{categoryName}")
	public ResponseEntity<List<ProductDTO>> listaStavkiPoKategoriji(@PathVariable String categoryName) {

		List<Product> products = productService.findAll();
		ArrayList<ProductDTO> dtos = new ArrayList<>();

		for (Product product : products) {
			if (product.getCategory().getName().equalsIgnoreCase(categoryName))
				dtos.add(new ProductDTO(product));
		}
		
		Collections.reverse(dtos);
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<ProductDTO> getOneProduct(@PathVariable Long id) {
		Optional<Product> product = productService.findOne(id);

		if (!product.isPresent())
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		ProductDTO productDTO = new ProductDTO(product.get());
		return new ResponseEntity<>(productDTO, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<ProductDTO> create(@RequestBody ProductDTO productDTO) throws Exception {
		Product product = productMapper.dtoToEntity(productDTO);
		Product savedProduct = productService.save(product);
		return new ResponseEntity<>(productMapper.entityToDto(savedProduct), HttpStatus.CREATED);
	}

}
