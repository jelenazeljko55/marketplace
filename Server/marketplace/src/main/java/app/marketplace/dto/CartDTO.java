package app.marketplace.dto;

import java.util.ArrayList;
import java.util.List;

import app.marketplace.model.Cart;
import app.marketplace.model.ProductQuantity;

public class CartDTO {

	private List<ProductQuantityDTO> productQuantityDTOs;
	private Long id;
	
	public CartDTO() { }
	
	public CartDTO(Cart cart) {
		List<ProductQuantityDTO> productQuantityDTOs = new ArrayList<ProductQuantityDTO>();
		for (ProductQuantity productQuantity : cart.getProductQuantities()) {
			ProductQuantityDTO productQuantityDTO = new ProductQuantityDTO();
			productQuantityDTO.setProductId(productQuantity.getProduct().getId());
			productQuantityDTO.setProductName(productQuantity.getProduct().getName());
			productQuantityDTO.setProductUrl(productQuantity.getProduct().getPictureUrl());
			productQuantityDTO.setQuantityInCart(productQuantity.getQuantity());
			productQuantityDTOs.add(productQuantityDTO);
		}
		this.productQuantityDTOs = productQuantityDTOs;
		this.id = cart.getId();
	}

	public List<ProductQuantityDTO> getProductQuantityDTOs() {
		return productQuantityDTOs;
	}
	public void setProductQuantityDTOs(List<ProductQuantityDTO> productQuantityDTOs) {
		this.productQuantityDTOs = productQuantityDTOs;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	
	
}
