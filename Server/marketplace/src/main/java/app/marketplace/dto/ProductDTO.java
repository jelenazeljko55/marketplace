package app.marketplace.dto;

import app.marketplace.model.Product;

public class ProductDTO {

	private Long id;

	private String name;

	private String pictureUrl;

	// private List<String> picture;

	private int quantity;

	private String description;
	
	private String dateAndTime;

	private double price;

	private CategoryDTO category;

	public ProductDTO(Product product) {
		super();
		this.id = product.getId();
		this.name = product.getName();
		this.pictureUrl = product.getPictureUrl();
		this.quantity = product.getQuantity();
		this.description = product.getDescription();
		this.price = product.getPrice();
		this.dateAndTime = product.getDateTime().toString();
		this.category = new CategoryDTO(product.getCategory());
	}

	public ProductDTO(Long id, String name, String pictureUrl, int quantity, String description, double price,
			CategoryDTO category) {
		super();
		this.id = id;
		this.name = name;
		this.pictureUrl = pictureUrl;
		this.quantity = quantity;
		this.description = description;
		this.price = price;
		this.category = category;
	}

	public ProductDTO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public CategoryDTO getCategory() {
		return category;
	}

	public void setCategory(CategoryDTO category) {
		this.category = category;
	}

	public String getDateAndTime() {
		return dateAndTime;
	}

	public void setDateAndTime(String dateAndTime) {
		this.dateAndTime = dateAndTime;
	}

	
}
