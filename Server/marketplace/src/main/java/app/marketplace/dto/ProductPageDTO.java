package app.marketplace.dto;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;

import app.marketplace.model.Product;

public class ProductPageDTO {

	private List<ProductDTO> products;
	private boolean lastPage;

	public ProductPageDTO(Page<Product> productsPage) {
		lastPage = productsPage.isLast();
		products = productsPage.getContent().stream().map(ProductDTO::new).collect(Collectors.toList());
		Collections.reverse(products);

	}

	public List<ProductDTO> getProducts() {
		return products;
	}

	public void setProducts(List<ProductDTO> products) {
		this.products = products;
	}

	public boolean isLastPage() {
		return lastPage;
	}

	public void setLastPage(boolean lastPage) {
		this.lastPage = lastPage;
	}

}
