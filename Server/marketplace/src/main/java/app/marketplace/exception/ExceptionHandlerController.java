package app.marketplace.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {	
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<?> handleException(Exception e) { 
		return new ResponseEntity<String>("An error occurred while sending request. ", HttpStatus.BAD_REQUEST);
	}
	
}

