package app.marketplace.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import app.marketplace.dto.CategoryDTO;
import app.marketplace.model.Category;

@Component
public class CategoryMapper implements Mapper<Category, CategoryDTO> {

	@Override
	public Category dtoToEntity(CategoryDTO dto) {
		Category category = new Category();
		category.setId(dto.getId());
		category.setName(dto.getName());
		category.setPictureUrl(dto.getPictureUrl());
		category.setDescription(dto.getDescription());
		return category;
	}

	@Override
	public CategoryDTO entityToDto(Category entity) {
		CategoryDTO dto = new CategoryDTO();
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		dto.setPictureUrl(entity.getPictureUrl());
		dto.setDescription(entity.getDescription());
		return dto;
	}

	@Override
	public List<CategoryDTO> entitiesToDtos(List<Category> entities) {
		List<CategoryDTO> dtos = entities.stream().map(e -> entityToDto(e)).collect(Collectors.toList());	
		return dtos;
	}


}
