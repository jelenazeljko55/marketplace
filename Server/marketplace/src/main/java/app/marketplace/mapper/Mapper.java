package app.marketplace.mapper;

import java.util.List;

public interface Mapper<E, D> {

    E dtoToEntity(D dto);

    D entityToDto(E entity);
    
    List<D> entitiesToDtos(List<E> entities);
    
}
