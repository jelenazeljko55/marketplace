package app.marketplace.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import app.marketplace.dto.CategoryDTO;
import app.marketplace.dto.ProductDTO;
import app.marketplace.model.Category;
import app.marketplace.model.Product;

@Component
public class ProductMapper implements Mapper<Product, ProductDTO> {

	@Autowired
	CategoryMapper categoryMapper;
	
	@Override
	public Product dtoToEntity(ProductDTO dto) {
		Product product = new Product();
		product.setId(dto.getId());
		product.setName(dto.getName());
		product.setPictureUrl(dto.getPictureUrl());
		product.setDescription(dto.getDescription());
		product.setQuantity(dto.getQuantity());
		product.setPrice(dto.getPrice());
		product.setCategory(categoryMapper.dtoToEntity(dto.getCategory()));
		return product;
	}

	@Override
	public ProductDTO entityToDto(Product entity) {
		ProductDTO dto = new ProductDTO();
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		dto.setPictureUrl(entity.getPictureUrl());
		dto.setDescription(entity.getDescription());
		dto.setQuantity(entity.getQuantity());
		dto.setPrice(entity.getPrice());
		dto.setCategory(categoryMapper.entityToDto(entity.getCategory()));
		return dto;
	}

	@Override
	public List<ProductDTO> entitiesToDtos(List<Product> entities) {
		List<ProductDTO> dtos = entities.stream().map(e -> entityToDto(e)).collect(Collectors.toList());	
		return dtos;
	}
	
	
}
