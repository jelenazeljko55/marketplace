package app.marketplace.model;

public class AddressData {

	private String email;
	
	private String name;
	
	private String address;
	
	private int houseNumber;
	
	private String city;
	
	public AddressData(String email, String name, String address, int houseNumber, String city) {
		super();
		this.email = email;
		this.name = name;
		this.address = address;
		this.houseNumber = houseNumber;
		this.city = city;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(int houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	
}
