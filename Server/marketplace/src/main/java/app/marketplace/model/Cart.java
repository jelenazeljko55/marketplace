package app.marketplace.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
public class Cart {
	
	@Id
	@GeneratedValue
	private Long id;	
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<ProductQuantity> productQuantities = new HashSet<ProductQuantity>();

	public Cart(Long id, Set<ProductQuantity> productQuantities) {
		super();
		this.id = id;
		this.productQuantities = productQuantities;
	}

	public Cart() { }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<ProductQuantity> getProductQuantities() {
		return productQuantities;
	}

	public void setProductQuantities(Set<ProductQuantity> productQuantities) {
		this.productQuantities = productQuantities;
	}

	
}
