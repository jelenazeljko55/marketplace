package app.marketplace.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.*;

@Entity
public class Product {

	@Id
	@GeneratedValue
	private Long id;		
	
	private String name;
	
	private String pictureUrl;
	
	//private List<String> picture;
	
	private int quantity;
	
	private String description;
	
	private double price;
	
	private boolean deleted;
	
	private LocalDateTime dateTime;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Category category;

	public Product(Long id, String name, String pictureUrl, int quantity, String description, double price,
			boolean deleted, LocalDateTime dateTime, Category category) {
		super();
		this.id = id;
		this.name = name;
		this.pictureUrl = pictureUrl;
		this.quantity = quantity;
		this.description = description;
		this.price = price;
		this.deleted = deleted;
		this.dateTime = dateTime;
		this.category = category;
	}
	
	

	public Product(String name, String pictureUrl, int quantity, String description, double price, boolean deleted,
			LocalDateTime dateTime, Category category) {
		super();
		this.name = name;
		this.pictureUrl = pictureUrl;
		this.quantity = quantity;
		this.description = description;
		this.price = price;
		this.deleted = deleted;
		this.dateTime = dateTime;
		this.category = category;
	}



	public Product() { }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}

	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	
}
