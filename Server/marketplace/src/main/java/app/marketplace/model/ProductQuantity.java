package app.marketplace.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class ProductQuantity {

	@Id
	@GeneratedValue
	private Long id;	
	
	private int quantity;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Product product;

	public ProductQuantity(Long id, int quantity, Product product) {
		super();
		this.id = id;
		this.quantity = quantity;
		this.product = product;
	}
	
	public ProductQuantity(int quantity, Product product) {
		this.quantity = quantity;
		this.product = product;
	}

	public ProductQuantity() { }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	
}
