package app.marketplace.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import app.marketplace.model.Cart;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {

	
	
	
	
}
