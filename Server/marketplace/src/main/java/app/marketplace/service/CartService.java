package app.marketplace.service;

import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.hibernate.internal.build.AllowSysOut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.marketplace.model.Cart;
import app.marketplace.model.Product;
import app.marketplace.model.ProductQuantity;
import app.marketplace.repository.CartRepository;
import app.marketplace.repository.ProductQuantityRepository;
import app.marketplace.repository.ProductRepository;

@Service
public class CartService {

	@Autowired
	public CartRepository cartRepository;
	
	@Autowired
	public ProductRepository productRepository;
	
	@Autowired
	public ProductQuantityRepository productQuantityRepository;

	public Long addToCart(Long productId, Long cartId) {
		
		Optional<Cart> optionalCart = cartRepository.findById(cartId);
		if (optionalCart.isPresent()) {
			Cart cart = optionalCart.get();
			for (ProductQuantity pq : cart.getProductQuantities()) {
				if (pq.getProduct().getId() == productId) {
					pq.setQuantity(pq.getQuantity() + 1);
					
					Cart savedCart = cartRepository.save(cart);	
					return savedCart.getId();
				}		
			}	
			Product addedProduct = productRepository.findById(productId).get();
			ProductQuantity productQuantity = new ProductQuantity(1, addedProduct);
			cart.getProductQuantities().add(productQuantity);	
			
			Cart savedCart = cartRepository.save(cart);	
			return savedCart.getId();				
		}
		
		Cart newCart = new Cart();
		Product addedProduct = productRepository.findById(productId).get();
		ProductQuantity productQuantity = new ProductQuantity(1, addedProduct);
		productQuantityRepository.save(productQuantity);
		newCart.getProductQuantities().add(productQuantity);
		Cart savedCart = cartRepository.save(newCart);	
		return savedCart.getId();					
	}

	public Cart getById(Long id) {	
		return cartRepository.getById(id);
	}

	public String buy(Long cartId) {
		Cart cart = cartRepository.findById(cartId).get();
		
		StringBuilder message = new StringBuilder("Kupljeno: ");
		for (ProductQuantity pq : cart.getProductQuantities()) {
			Product product = productRepository.findById(pq.getProduct().getId()).get();

			if (pq.getQuantity() <= product.getQuantity()) {
				System.out.println("kolicina u skladistu: " + product.getQuantity());
				System.out.println("kolicina u korpi: " + pq.getQuantity());
				product.setQuantity(product.getQuantity() - pq.getQuantity());
				System.out.println("novo u stanje u skladistu: " + product.getQuantity());
				productRepository.save(product); //cuvam izmenjen product
				message.append("  " + product.getName() + " \n");
			}			
		}	
		return message.toString();
	}
	
	
	
}
