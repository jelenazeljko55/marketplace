package app.marketplace.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.marketplace.model.Category;
import app.marketplace.repository.CategoryRepository;

@Service
public class CategoryService {

	@Autowired
	CategoryRepository categoryRepository;

	public List<Category> findAll() {
		return categoryRepository.findAll();
	}

	public Page<Category> findAll(Pageable page) {
		return categoryRepository.findAll(page);
	}

	public Optional<Category> findOne(Long id) {
		return categoryRepository.findById(id);
	}

	public void remove(Category category) {
		categoryRepository.delete(category);
	}

	public List<Category> findByNameContaining(String name) {
		return categoryRepository.findByNameContaining(name);
	}
	
	public Category save(Category category) {
		return categoryRepository.save(category);
	} 

}
