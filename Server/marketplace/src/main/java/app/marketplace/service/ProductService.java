package app.marketplace.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.marketplace.model.Product;
import app.marketplace.repository.ProductRepository;

@Service
public class ProductService {

	@Autowired
	ProductRepository productRepository;

	public Product save(Product product) {
		product.setDateTime(LocalDateTime.now());
		return productRepository.save(product);
	}

	public List<Product> findAll() {
		return productRepository.findAllByDeleted(false);
	}

	public Page<Product> findAll(Pageable page) {
		return productRepository.findAll(page);
	}

	public Optional<Product> findOne(Long id) {
		return productRepository.findById(id);
	}

	public void remove(Product product) {
		productRepository.delete(product);
	}

	public List<Product> findByNameContaining(String name) {
		return productRepository.findByNameContaining(name);
	}
}
